interface IWrapper {
  width: number;
  height: number;
  color: string;
}

const Wrapper = function(): IWrapper {
  return {
    width: window.document.getElementById("wrapper").offsetWidth,
    height: window.document.getElementById("wrapper").offsetHeight,
    color: "#C59B76"
  };
};

export default Wrapper;
