import Wrapper from "./wrapper";

interface IProperties {
  x: number;
  y: number;
  radius: number;
}

interface ICircle {
  getProperties: () => IProperties;
  setRadius: (value: number) => void;
}
const Circle = function(): ICircle {
  const wrapper = Wrapper();

  const circleProps: IProperties = {
    x: wrapper.width / 2,
    y: wrapper.height / 2,
    radius: 20
  };

  return {
    getProperties(): IProperties {
      return circleProps;
    },
    setRadius(value): void {
      circleProps.radius = value;
    }
  };
};

export default Circle;
