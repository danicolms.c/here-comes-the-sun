import p5 from "p5";
import Wrapper from "./wrapper";
import Circle from "./circle";
import Line from "./line";

const sketch = function(p: p5) {
  const wrapper = Wrapper();

  const circleInst = Circle();

  const bigLineInst = Line();
  const smallLineInst = Line();

  smallLineInst.setLength(250);
  bigLineInst.setLength(300);

  smallLineInst.setFinalPositions();
  bigLineInst.setFinalPositions();

  p.setup = function(): any {
    p.createCanvas(wrapper.width, wrapper.height);
    p.background(wrapper.color);

    p.noFill();
    p.strokeWeight(4);
    p.stroke("#FFB238");
    p.circle(
      circleInst.getProperties().x,
      circleInst.getProperties().y,
      circleInst.getProperties().radius
    );
  };

  p.draw = function(): any {
    smallLineInst.setFinalPositions();
    bigLineInst.setFinalPositions();

    p.strokeWeight(2);
    p.stroke("#FABC3C");
    p.line(
      smallLineInst.getProperties().startX,
      smallLineInst.getProperties().startY,
      smallLineInst.getProperties().finalX,
      smallLineInst.getProperties().finalY
    );

    p.stroke("#FAB32A");
    p.line(
      bigLineInst.getProperties().startX,
      bigLineInst.getProperties().startY,
      bigLineInst.getProperties().finalX,
      bigLineInst.getProperties().finalY
    );

    smallLineInst.setAngle(smallLineInst.getProperties().angle + 10);
    bigLineInst.setAngle(bigLineInst.getProperties().angle + 1);
  };
};

function startp5(): p5 {
  return new p5(sketch, window.document.getElementById("wrapper"));
}

startp5();
