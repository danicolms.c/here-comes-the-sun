import Wrapper from "./wrapper";

import p5 from "p5";
interface IProperties {
  startX: number;
  startY: number;
  finalX: number;
  finalY: number;
  angle: number;
  length: number;
}

interface ILinePosition {
  startX: number;
  startY: number;
  finalX: number;
  finalY: number;
  angle: number;
  length: number;
}
interface ILine {
  getProperties: () => ILinePosition;

  setLength: (value: number) => void;
  setAngle: (value: number) => void;
  setFinalPositions: () => void;
}

interface IFinalPositions {
  x: number;
  y: number;
  z: number;
}
const Line = function(): ILine {
  const wrapper = Wrapper();
  const lineProps: IProperties = {
    startX: wrapper.width / 2,
    startY: wrapper.height / 2,
    finalX: null,
    finalY: null,

    angle: 0,
    length: 300
  };

  return {
    getProperties(): ILinePosition {
      return {
        startX: lineProps.startX,
        startY: lineProps.startY,
        finalX: lineProps.finalX,
        finalY: lineProps.finalY,
        angle: lineProps.angle,
        length: lineProps.length
      };
    },

    setLength(value): void {
      lineProps.length = value;
    },
    setAngle(value): void {
      lineProps.angle = value;
    },
    setFinalPositions(): void {
      const finalPositions: IFinalPositions = p5.Vector.fromAngle(
        lineProps.angle,
        lineProps.length
      );

      lineProps.finalX = finalPositions.x + lineProps.startX;
      lineProps.finalY = finalPositions.y + lineProps.startY;
    }
  };
};

export default Line;
